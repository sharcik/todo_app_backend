Rails.application.routes.draw do

  post 'auth_user' => 'authentication#authenticate_user'
  post 'create_task' => 'tasks#create'
  get 'home' => 'home#index'
  post 'get_tasks' => 'tasks#index'
  post 'change_status' => 'tasks#change_status'
  post 'delete_tasks' => 'tasks#destroy'
  post 'user_data' => 'authentication#user_data'

end


