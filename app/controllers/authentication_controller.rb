class AuthenticationController < ApplicationController
  before_action :authenticate_request!, :only => [:user_data]

  def authenticate_user
    if User.where(email: params[:username]).present?
      user = User.find_for_database_authentication(email: params[:username])
      if user and user.valid_password?(params[:password])
        render json: payload(user)
      else
        render json: {errors: ['Invalid Username/Password']}, status: :unauthorized
      end
    else
      user = User.new(email: params[:username], password: params[:password], password_confirmation: params[:confirm])
      if user.save
        render json: payload(user) 
      else
        render :json => { :message => 'not correct login or password' }
      end
    end
  end

  def user_data
    user = User.find(current_user)
    render json:{ user: { id: user.id, email: user.email } }
  end

  private 

  def payload(user)
    return nil unless user and user.id
    {
      auth_token: JsonWebToken.encode({user_id: user.id}),
      user: {id: user.id, email: user.email}
    }
  end


end
