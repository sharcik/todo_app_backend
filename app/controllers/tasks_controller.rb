class TasksController < ApplicationController
  before_action :authenticate_request!

  def index
    user = User.find(current_user)
    @tasks = user.tasks

    render :json => { :tasks => @tasks }
  end

  def create
    user = User.find(current_user)
    @task = Task.new(title: params[:task], user_id: user.id)

    if @task.save
      @tasks = user.tasks
      render :json => { :tasks => @tasks }
    else
      render :json => { :message => 'error creationg' }
    end

  end

  def change_status
    task = Task.find(params[:task])
    user = User.find(current_user)
    
    task.status == false ? task.status = true : task.status = false

    if task.save
      @tasks = user.tasks
      render :json => { :tasks => @tasks }
    else
      render :json => { :message => 'status not changed' }
    end  
  end

  def destroy
    user = User.find(current_user)
    tasks = user.tasks.where(status: true) if user.tasks.where(status: true)

    if tasks.destroy_all
      @tasks = user.tasks
      render :json => { :tasks => @tasks }
    else
      render :json => { :message => 'something went wrong' }
    end  
  end

end
